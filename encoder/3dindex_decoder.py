#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import statistics
import sys
import os.path
from collections import Counter
from collections import OrderedDict
import oligoutil
import settings

if len(sys.argv) != 4:
    print('./decode <fileid map file> <data freq file> <oligofile>\n')
    sys.exit(1)

def decode_data(oligo_file, ternary_huffman):
    data_strings = []
    encoded_map = {}
    ndecode_failure = 0 
    nhuffman_failure = 0
    nfileid_failure = ncolid_failure = 0
    with open(oligo_file, 'r') as f:
        for line in f:
            #remove primers
            oligo_str = line.rstrip()
            oligoutil.dprint("Processing oligo: " + oligo_str)

            #assert oligo_str[:settings.G_5PRIMER_LEN] == settings.G_5_PRIMER
            #assert oligo_str[-settings.G_3PRIMER_LEN:] == settings.G_3_PRIMER
            #oligo_str = oligo_str[settings.G_5PRIMER_LEN :
            #        -(settings.G_3PRIMER_LEN)]
            #oligoutil.dprint("Processing oligo:" + oligo_str)

            table_primer = oligo_str[:settings.G_TABLE_PRIMER_LEN]
            if table_primer in settings.G_TABLE_PRIMERS:
                table_index = settings.G_TABLE_PRIMERS.index(table_primer)
            else:
                oligoutil.dprint("ERROR: Table primer not found: " + table_primer)
                continue

            db_primer = oligo_str[-settings.G_DB_PRIMER_LEN:]
            if db_primer in settings.G_DB_PRIMERS:
                db_index = settings.G_DB_PRIMERS.index(db_primer)
                oligoutil.dprint("DBID: " + str(db_index))
            else:
                oligoutil.dprint("ERROR: dbprimer not found: " + db_primer)
                continue
            oligo_str = oligo_str[settings.G_TABLE_PRIMER_LEN:
                    -settings.G_DB_PRIMER_LEN]

            #remove col primer 
            col_primer = oligo_str[-settings.G_COL_PRIMER_LEN:]
            if col_primer in settings.G_COL_PRIMERS:
                col_index = settings.G_COL_PRIMERS.index(col_primer)
            else:
                oligoutil.dprint("ERROR: colprimer not found: " + col_primer)
                continue

            oligo_str = oligo_str[:-settings.G_COL_PRIMER_LEN]

            #remove sense
            oligo_str = oligo_str[1:-1]

            #decode metadata (direction trit, fileid trits, colid trits, parity trit)
            metadata_len = 1 + settings.G_FILEID_CHARS + settings.G_COLID_CHARS + 1
            oligo_metadata = oligo_str[40:40 + metadata_len]
            oligoutil.dprint(oligo_metadata)
            try:
                encoded_metadata = oligoutil.decode_oligo(oligo_metadata)
            except:
                oligoutil.dprint("ERROR: metadata decoding failed.")
                ndecode_failure += 1
                continue

            oligoutil.dprint(encoded_metadata)

            #get and remove file id
            fileid_str = encoded_metadata[1:(1 + settings.G_FILEID_CHARS)]
            oligoutil.dprint(fileid_str)
            if len(fileid_str) != settings.G_FILEID_CHARS:
                continue
            
            fileid = oligoutil.todecimal(fileid_str) 
            if fileid != table_index:
                print("ERROR: Fileid mismatch encountered " + str(fileid) + " - " +
                        str(table_index))
                continue
                #fileid = table_index

            #use fileid to fix table index if we dont have a table index
            #if table_index == -1:
            #    table_index = fileid
            #else:
            #    fileid = table_index

            oligoutil.dprint("Final FILEID: " + str(fileid))

            #get and remove col id
            colid_str = encoded_metadata[-(1 + settings.G_COLID_CHARS):-1]
            if len(colid_str) != settings.G_COLID_CHARS:
                continue

            colid = oligoutil.todecimal(colid_str) 
            if colid != col_index:
                print("ERROR: Colid mismatch encountered " + str(colid) + 
                        "-" + str(col_index))
                ncolid_failure += 1
                #colid = col_index
                continue

            oligoutil.dprint("COLID: " + colid_str + " - " + str(colid))
            
            #use colid to fix col index if we dont have col index
            #if col_index == -1:
            #    col_index = colid
            #else:
            #    colid = col_index

            oligoutil.dprint("Final COLID: " + str(colid))

            #decode data
            oligo_str = oligo_str[0:40] + oligo_str[40 + metadata_len: ]
            oligoutil.dprint(oligo_str)
            if (encoded_metadata[0] == '1'):
                oligo_str = oligoutil.rcomplement(oligo_str)                
                oligoutil.dprint("reverse complimenting reversed oligo:" + oligo_str)
            try:
                encoded_data = oligoutil.decode_oligo(oligo_str)
            except:
                oligoutil.dprint("ERROR: data decoding failed.")
                ndecode_failure += 1
                continue

            oligoutil.dprint(encoded_data)

            #get base3 length
            base3_str = encoded_data[-settings.G_NBASE3_LEN_CHARS:]
            length = oligoutil.todecimal(base3_str)
 
            # check and remove parity
            parity_str = encoded_metadata[-1:]
            oligoutil.dprint(parity_str)
            encoded_metadata = encoded_metadata[:-1]
            if oligoutil.compute_parity(encoded_data + encoded_metadata) != parity_str:
                print('ERROR: Parity verification failed.')
                print('oligo:' + line.rstrip())
                print('decode data:' + encoded_data)
                print('decode metadata:' + encoded_metadata)
                print('computed parity: ' +
                        str(oligoutil.compute_parity(encoded_data + encoded_metadata)))
                print('stored parity: ' + parity_str)

                if (length > 40):
                    print('Skipping oligo as Huffman length is ' + str(length))
                    continue
                else:
                    print('Inspite of parity, attempting recovery as huffman length is only ' + str(length))
  
            data_strings.append(encoded_data + fileid_str)

            #remove base3 len chars
            encoded_data = encoded_data[: -settings.G_NBASE3_LEN_CHARS]

            #remove padding
            encoded_data = encoded_data[0 : length]
        
            #huffman decode 
            oligoutil.dprint('Huffman string: ' + encoded_data)

            try:
                decoded_str = str(colid) + '|' + ''.join(ternary_huffman.decode(encoded_data))
            except:
                nhuffman_failure += 1
                oligoutil.dprint("ERROR: huffman decoding failed.")
                continue
 
            oligoutil.dprint('Decoded str: ' + decoded_str)
            #uid = str(db_index) + '-' + str(table_index)
            uid = str(db_index) + '-' + str(fileid)
            if uid not in encoded_map:
                encoded_map[uid] = []

            encoded_map[uid].append(decoded_str)

    return encoded_map

def decode_dict(dict_strings, ternary_huffman):
    segments = {}

    for encoded_str in dict_strings:
        #get fileid
        fileid_str = encoded_str[-settings.G_FILEID_CHARS:]
        fileid = oligoutil.todecimal(fileid_str) 
        encoded_str = encoded_str[: -settings.G_FILEID_CHARS]

        #get base3 length
        base3_str = encoded_str[-settings.G_NBASE3_LEN_CHARS:]
        length = oligoutil.todecimal(base3_str)
        encoded_str = encoded_str[:-settings.G_NBASE3_LEN_CHARS]

        # get index
        index_str = encoded_str[-settings.G_MAX_INDEX_LEN:]
        chunk_idx = oligoutil.todecimal(index_str)
        encoded_str = encoded_str[:-settings.G_MAX_INDEX_LEN]

        #remove padding
        encoded_str = encoded_str[0 : length]
        
        #add string at index
        if chunk_idx in segments:
            assert segments[chunk_idx] == encoded_str

        segments[chunk_idx] = encoded_str

    #now we have all segments. merge it all and decode
    huffman_str = ''.join(segments[x] for x in sorted(segments))
    oligoutil.dprint('Length of encoded str: ' + str(len(huffman_str)))

    decoded_bytes = ternary_huffman.decode(huffman_str)

    return bytearray(int(c) for c in decoded_bytes)

data_freq_file = ''
fileid_map_file = ''
oligo_file = ''
for arg in sys.argv[1:]:
    if 'data.freqs' in arg:
        data_freq_file = arg
    elif 'map' in arg:
        fileid_map_file = arg
    elif 'oligos' in arg:
        oligo_file = arg
    else:
        print('Invalid file ' + arg)
        sys.exit(1)

#load huffman dictionary
print('Loading huffman for data file: ' + data_freq_file)
data_freqs, ternary_huffman_data = oligoutil.load_huffman_dictionary(data_freq_file)

#load file id map
print('Loading fileid map from: ' + fileid_map_file)
fileid_map = oligoutil.deserialize_dictionary(fileid_map_file)
print(fileid_map)

#now decode the oligo file line at a time
decoded_data = decode_data(oligo_file, ternary_huffman_data)

#flush out data files
data_files = []
path_name = os.path.dirname(sys.argv[1]) 
for key, value in decoded_data.items():
    print('Looking up key ' + key)
    if key not in fileid_map:
        print('Key ' + key + ' not found')
        continue
    base_name = os.path.basename(fileid_map[key])
    data_files.append(base_name[0:base_name.find('.')])
    file_name = path_name + '/' + fileid_map[key] + '.recovered'
    print('Writing decoded file: ' + file_name)
    with open(file_name, 'w') as f:
        for line in value:
            f.write(line + '\n')
