#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Encoding format
# Data 
# ----
# | SENSE | data G_NHUFFMAN_CHARS | len G_NBASE3_LEN | SENSE |
#

import statistics
import sys
import os.path
from collections import Counter
import oligoutil
import settings
import pandas as pd
import getopt

if len(sys.argv) < 2:
    print('./encode <filename>\n')
    sys.exit(1)

def huffman_to_oligo(input_str, col_id, file_id, db_id):
    huffman_len = len(input_str)
    huffman_str = input_str.ljust(settings.G_NHUFFMAN_CHARS, '0')

    # get base3 length, pad it, and append it to huffman string
    base3_str = oligoutil.tobase3(huffman_len)
    assert len(base3_str) <= settings.G_NBASE3_LEN_CHARS
    base3_str = base3_str.rjust(settings.G_NBASE3_LEN_CHARS, '0')
    encoded_str = huffman_str + base3_str    
    rencoded_str = encoded_str
    oligo_str = oligoutil.generate_oligo(encoded_str)
    roligo_str = oligoutil.rcomplement(oligo_str)
            
    # one trit to indicate forward mirror
    metadata_str = '0'
    encoded_str += metadata_str

    # convert file id to trits
    fileid_str = oligoutil.tobase3(file_id)
    assert len(fileid_str) <= settings.G_FILEID_CHARS
    fileid_str = fileid_str.rjust(settings.G_FILEID_CHARS, '0')
    metadata_str += fileid_str
    encoded_str += fileid_str

    # convert cold id to trits
    colid_str = oligoutil.tobase3(col_id)
    assert len(fileid_str) <= settings.G_COLID_CHARS
    colid_str = colid_str.rjust(settings.G_COLID_CHARS, '0')
    metadata_str += colid_str
    encoded_str += colid_str

    #compute parity trit
    metadata_str += oligoutil.compute_parity(encoded_str)
           
    #convert meatdata to oligo
    metadata_oligo_str = oligoutil.generate_oligo(metadata_str)
    oligo_str = oligoutil.add_3dprimer_sense(oligo_str, metadata_oligo_str,
            db_id, int(file_id), int(col_id), True)

    #now do the same for reverse complement, but use same fileid
    rmetadata_str = '1'
    rencoded_str += rmetadata_str

    rmetadata_str += fileid_str
    rencoded_str += fileid_str
    rmetadata_str += colid_str
    rencoded_str += colid_str

    rmetadata_str += oligoutil.compute_parity(rencoded_str)

    rmetadata_oligo_str = oligoutil.generate_oligo(rmetadata_str)
    roligo_str = oligoutil.add_3dprimer_sense(roligo_str, rmetadata_oligo_str,
            db_id, int(file_id), int(col_id), True)

    return oligo_str, roligo_str

def data_encode(input_name, db_id, file_id, ternary_huffman):
    oligos=[]
    line_len = []
    total_huffman_len = 0
    df = pd.read_csv(input_name, sep='|', header=None, index_col=False, dtype="string")
    for col in df:
        print("Processing col " + str(col))
        print(df[col].tolist())
        items = []
        for item in df[col].tolist():
            item_str = item + "|"
            huffman_str = ternary_huffman.encode(item_str)
            huffman_len = len(huffman_str)
            if total_huffman_len + huffman_len >= settings.G_NHUFFMAN_CHARS:
                oligoutil.dprint('Converting next ' + str(len(items)) + ' items into an oligo')
                merged_item_str = '|'.join(items)
                print(merged_item_str)
                merged_huffman_str = ternary_huffman.encode(merged_item_str)
                merged_huffman_len = len(merged_huffman_str)
                oligoutil.dprint('merged huffman length: ' + str(merged_huffman_len))
                line_len.append(merged_huffman_len)
                assert merged_huffman_len <= settings.G_NHUFFMAN_CHARS

                merged_oligo_str, reverse_oligo = huffman_to_oligo(merged_huffman_str,
                        col, file_id, db_id)

                oligos.append(merged_oligo_str)
                oligos.append(reverse_oligo)
                oligoutil.dprint('merged oligo length: ' + str(len(merged_oligo_str)))
                total_huffman_len = 0
                items.clear()

            total_huffman_len += huffman_len
            items.append(item)

        if items:
            oligoutil.dprint('Converting next ' + str(len(items)) + ' items into an oligo')
            merged_item_str = '|'.join(items)
            print(merged_item_str)
            merged_huffman_str = ternary_huffman.encode(merged_item_str)
            merged_huffman_len = len(merged_huffman_str)
            oligoutil.dprint('merged huffman length: ' + str(merged_huffman_len))
            line_len.append(merged_huffman_len)
            assert merged_huffman_len <= settings.G_NHUFFMAN_CHARS

            merged_oligo_str, reverse_oligo = huffman_to_oligo(merged_huffman_str, col, file_id, db_id)

            oligos.append(merged_oligo_str)
            oligos.append(reverse_oligo)
            oligoutil.dprint('merged oligo length: ' + str(len(merged_oligo_str)))
            total_huffman_len = 0
            items.clear()
            
    print('Done encoding file ' + input_name)
    oligoutil.dprint('Huffman code min length: ' + str(min(line_len)) + ', max: ' +
            str(max(line_len)) + ', median: ' +
            str(statistics.median(line_len)))

    return oligos

data_files = []
for arg in sys.argv[1:]:
    data_files.append(arg)

print('Building huffman DS for data files');
data_freqs, ternary_huffman_data = oligoutil.build_huffman_line_based(data_files)

all_oligos=[]
file_ids = [0] * settings.G_MAX_DBS
fileid_map = {}
for arg in sys.argv[1:]:
    fname = os.path.basename(arg)
    print(fname)
    db_id = int(settings.G_DBMAP[fname[0:3]])
    print('Encoding data for file ' + os.path.basename(arg) + " db_id = " +
            str(db_id) + ", fileid = " + str(file_ids[db_id]))
    fileid_map[str(db_id) + "-" + str(file_ids[db_id])] = fname
    oligos = data_encode(arg, db_id, file_ids[db_id], ternary_huffman_data)

    print('File ' + arg + ' generated ' + str(len(oligos)) + ' oligos.')
    all_oligos.extend(oligos)
    file_ids[db_id] += 1

if [oligo for oligo in all_oligos if len(oligo) != len(all_oligos[0])]:
    print('ERROR: Not all oligos same in length')
    sys.exit(1)

#add redundancy

#print stats
min_gc = 1
max_gc = 0
for i in range(len(all_oligos)):
#    print('oligo-' + str(i))
#    print('\tlength: ' + str(len(all_oligos[i])))
    counter = Counter(all_oligos[i])
    gc_content = (counter['G'] + counter['C'])/(counter['A'] +
            counter['G'] + counter['C'] + counter['T'])
    min_gc = gc_content if min_gc > gc_content else min_gc
    max_gc = gc_content if max_gc < gc_content else max_gc

#    print('\tGC content:' + str(gc_content))
    assert settings.G_GC_LOW <= gc_content <= settings.G_GC_HIGH

print("mingc: " + str(min_gc) + " maxgc: " + str(max_gc))

#generate op file name
input_files = []
for arg in sys.argv[1:]:
    base_name = os.path.basename(arg)
    input_files.append(base_name[:base_name.find('.')])

oligo_file_name = os.path.dirname(sys.argv[1]) + '/' + '-'.join(input_files) + '.oligos'

#flush oligos
print('Writing ' + str(len(all_oligos)) + ' oligos to file: ' + oligo_file_name);
oligo_file = open(oligo_file_name, 'w')
oligo_file.write('\n'.join(all_oligos))
oligo_file.close

#flush fileid_map
map_file_name = os.path.dirname(sys.argv[1]) + '/' + '-'.join(input_files) + '.map'
oligoutil.serialize_dictionary(fileid_map, map_file_name)

#flush data huffman frequency
data_freq_fname = os.path.dirname(sys.argv[1]) + '/' + '-'.join(input_files) + '.data.freqs'
oligoutil.serialize_dictionary(data_freqs, data_freq_fname)
