#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import editdistance
import pdb
from itertools import combinations

edmap = {}
len_threshold = 25

def get_next_primer(maxd_value, maxd_list, all_primers):
    found_primer = False
    for p in all_primers:
        if p in maxd_list:
            continue

        all_pass = True
        for mdp in maxd_list:
            if edmap[p][mdp] < maxd_value:
                all_pass = False
                break
            
        if all_pass:
            maxd_list.append(p)
            found_primer = True
            break

    if found_primer == True:
        if len(maxd_list) != len_threshold:
            maxd_list = get_next_primer(maxd_value, maxd_list, all_primers)

    return maxd_list


pfile = open("primer_list.txt", 'r')
data = pfile.read()
primers = data.splitlines()
pfile.close()
for p in primers:
    for q in primers:
        if p != q:
            ed = editdistance.eval(p, q)
            if p not in edmap:
                edmap[p] = {}

            if q not in edmap:
                edmap[q] = {}
            assert p not in edmap[q]
            edmap[q][p] = int(ed)


#for p in primers:
#    maxd_list = [p]
#    maxd_list = get_next_primer(3, maxd_list, primers)
#    print('max list length for ' + p + ' is ' + str(len(maxd_list)))
print("generating all combinations of " + str(len_threshold) + " primers.")
comb = combinations(range(0,len(primers)), len_threshold)
print("Checking eddist")
maxc = ''
max_edist = 0
for ic in comb:
    c = [primers[i] for i in ic]
    edist = 0
    for i in range(0, len(c)):
        for j in range(i + 1,len(c)):
            edist += edmap[c[i]][c[j]]

    if edist > max_edist:
        print("Found new comb with max edist: " + str(edist))
        print(maxc)
        max_edist = edist
        maxc = c
        min_dist = 5
        for p in c:
            for q in c:
                if p != q:
                    ed = editdistance.eval(p, q)
                    if min_dist > ed:
                        min_dist = ed;
                        min_pair = p + " - " + q
        print("Min edist: " + str(min_dist) + " min pair: " + min_pair)
 

print("Max edist: " + max_edist)
print("Maxc: " + maxc)


