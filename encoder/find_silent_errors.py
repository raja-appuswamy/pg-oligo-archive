#!/usr/bin/env python3
import sys
import settings

if len(sys.argv) < 3:
    print('./find_silent_errors.py <original-oligos> <samfile> <merged.fastq>')
    sys.exit(1)

oligo_map = {}

#bulid map assuming primers are already removed
with open(sys.argv[1]) as f:
    id = 1
    for line in f:
        oligo_str = line.rstrip()
        oligo_map[id] = oligo_str
        id += 1
print("Loaded " + str(len(oligo_map)) + " input oligos.")

#bulid map of readname to read string from fastq
read_map = {}
nreads = 0
with open(sys.argv[3]) as f:
    while True:
        read_name = f.readline()
        if not read_name:
            break

        read_str = f.readline().rstrip()
        f.readline()
        f.readline()
        read_name = read_name.rstrip().split()[0][1:]
        read_map[read_name] = read_str
        nreads += 1

print("Loaded " + str(nreads) + " input reads.")

nreads = 0
nunmapped = 0
nlen_err = 0
ndb_swap = 0
ndb_mismatch = 0
db_primer_map = {}
ndb_match = 0
ntable_swap = 0
table_primer_map = {}
ntable_mismatch = 0
ntable_match = 0
ncol_swap = 0
col_primer_map = {}
ncol_mismatch = 0
ncol_match = 0
with open(sys.argv[2]) as f:
    for line in f:
        nreads += 1
        fields = line.rstrip().split()
        read_name = fields[0]
        refoligo = fields[2]
        if refoligo == '*':
            nunmapped += 1
            continue

        ref_id = int((refoligo.split('_'))[1])
        
        assert ref_id in oligo_map
        if read_name not in read_map:
            #print(read_name + ' not found in map')
            nunmapped += 1
            continue

        oligo_str = oligo_map[ref_id]
        read_str = read_map[read_name]
        #print("Read: " + read_str)
        #print("Oligo: " + oligo_str)
        if len(read_str) != len(oligo_str):
            nlen_err += 1
            continue

        rtable_primer = read_str[:settings.G_TABLE_PRIMER_LEN]
        otable_primer = oligo_str[:settings.G_TABLE_PRIMER_LEN]
        if rtable_primer != otable_primer:
            if rtable_primer in settings.G_TABLE_PRIMERS:
                ntable_swap += 1
                if otable_primer not in table_primer_map:
                    table_primer_map[otable_primer] = {}
                if rtable_primer not in table_primer_map[otable_primer]:
                    table_primer_map[otable_primer][rtable_primer] = 0
                table_primer_map[otable_primer][rtable_primer] += 1
            else:
                ntable_mismatch += 1
        else:
            ntable_match += 1

        rdb_primer = read_str[-settings.G_DB_PRIMER_LEN:]
        odb_primer = oligo_str[-settings.G_DB_PRIMER_LEN:]
        if rdb_primer != odb_primer:
            if rdb_primer in settings.G_DB_PRIMERS:
                ndb_swap += 1
                if odb_primer not in db_primer_map:
                    db_primer_map[odb_primer] = {}
                if rdb_primer not in db_primer_map[odb_primer]:
                    db_primer_map[odb_primer][rdb_primer] = 0
                db_primer_map[odb_primer][rdb_primer] += 1
            else:
                ndb_mismatch += 1
        else:
            ndb_match += 1


        oligo_str = oligo_str[settings.G_TABLE_PRIMER_LEN:
                -settings.G_DB_PRIMER_LEN]
        read_str = read_str[settings.G_TABLE_PRIMER_LEN:
                -settings.G_DB_PRIMER_LEN]

        rcol_primer = read_str[-settings.G_COL_PRIMER_LEN:]
        ocol_primer = oligo_str[-settings.G_COL_PRIMER_LEN:]
        if rcol_primer != ocol_primer:
            if rcol_primer in settings.G_COL_PRIMERS:
                ncol_swap += 1
                if ocol_primer not in col_primer_map:
                    col_primer_map[ocol_primer] = {}
                if rcol_primer not in col_primer_map[ocol_primer]:
                    col_primer_map[ocol_primer][rcol_primer] = 0
                col_primer_map[ocol_primer][rcol_primer] += 1
            else:
                ncol_mismatch += 1
        else:
            ncol_match += 1

print("Final stats")
print("-----------")
print("Total reads : " + str(nreads))
print("Unmapped: " + str(nunmapped))
print("Length error: " + str(nlen_err))
print("DB match: " + str(ndb_match))
print("DB mismatch: " + str(ndb_mismatch))
print("DB swap: " + str(ndb_swap))
print("TABLE match: " + str(ntable_match))
print("TABLE mismatch: " + str(ntable_mismatch))
print("TABLE swap: " + str(ntable_swap))
print("COL match: " + str(ncol_match))
print("COL mismatch: " + str(ncol_mismatch))
print("COL swap: " + str(ncol_swap))

print("-----------------------")
print("DB Primer swap frequencies")
print("-----------------------")
for sprimer in db_primer_map:
    for tprimer in db_primer_map[sprimer]:
        print(sprimer + " to " + tprimer + " : " + str(db_primer_map[sprimer][tprimer]))

print("-----------------------")
print("Table Primer swap frequencies")
print("-----------------------")
for sprimer in table_primer_map:
    for tprimer in table_primer_map[sprimer]:
        print(sprimer + " to " + tprimer + " : " + str(table_primer_map[sprimer][tprimer]))

#print("-----------------------")
#print("Col Primer swap frequencies")
#print("-----------------------")
#for sprimer in col_primer_map:
#    for tprimer in col_primer_map[sprimer]:
#        print(sprimer + " to " + tprimer + " : " + str(col_primer_map[sprimer][tprimer]))
