#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import statistics
import sys
import os.path
from collections import Counter
from collections import OrderedDict
import oligoutil
import settings
import operator

def parse_oligos(oligo_file):
    valid_oligos = []
    nparity_failure = 0
    ndecode_failure = 0
    nfileid_failure = 0
    noligos = 0
    mezzanine_strings = []
    fwd_map = {}
    rev_map = {}
    with open(oligo_file, 'r') as f:
        for line in f:
            noligos += 1

            oligo_str = line.rstrip()
            oligo_str_cp = oligo_str
            oligoutil.dprint("Input oligo: " + oligo_str)

            # check sense
            if oligo_str[0] == 'G' or oligo_str[0] == 'C':
                if oligo_str[-1] != 'T' and oligo_str[-1] != 'A':
                    print("ERROR: senses failure.")
                    sys.exit(1)
                else:
                    print("ERROR: Reverse strand found.")
                    sys.exit(1)
            elif oligo_str[0] == 'T' or oligo_str[0] == 'A':
                if oligo_str[-1] != 'G' and oligo_str[-1] != 'C':
                    print("ERROR: senses failure.")
                    sys.exit(1)
                
            #remove sense
            oligo_str = oligo_str[1 :-1]

            #decode metadata (direction trit, fileid trits, parity trit)
            metadata_len = 1 + settings.G_FILEID_CHARS + 1
            oligo_metadata = oligo_str[40:40 + metadata_len]
            oligoutil.dprint("Raw Metadata: " + oligo_metadata)
            try:
                encoded_metadata = oligoutil.decode_oligo(oligo_metadata)
            except:
                print("ERROR: Mdata decode failed.")
                sys.exit(1)

            oligoutil.dprint("Encoded metadata: " + encoded_metadata)

            #get and remove file id
            fileid_str = encoded_metadata[1:-1]
            oligoutil.dprint("raw fileid: " + fileid_str)
            assert len(fileid_str) == settings.G_FILEID_CHARS
            fileid = oligoutil.todecimal(fileid_str) 
            if (fileid > 8):
                print("ERROR: pair found unexpected fileid " + str(fileid))
                sys.exit(1)

            #decode data
            oligo_str = oligo_str[0:40] + oligo_str[40 + metadata_len: ]
            oligoutil.dprint("raw data: " + oligo_str)

#            try:
            if (encoded_metadata[0] == '1'):
                oligo_str = oligoutil.rcomplement(oligo_str)

                #if oligo_str in rev_map:
                #    print("Found! " + oligo_str)
                #    assert oligo_str not in rev_map

                rev_map[oligo_str] = 1
                if oligo_str in fwd_map:
                    if line not in valid_oligos:
                        valid_oligos.append(line)
            else:
                fwd_map[oligo_str] = 1

                #only add forward oligos for which reverse pairs are found
                if oligo_str in rev_map:
                    if line not in valid_oligos:
                        valid_oligos.append(line)
#            except:
#                ndecode_failure += 1
#                continue

    print("From " + str(noligos) + " oligos, found " +
            str(ndecode_failure) + " decode failures, ")

    return valid_oligos

if len(sys.argv) != 2:
    print('./filter_oligos <input.oligo>\n')
    sys.exit(1)

#now decode the oligo file line at a time
oligo_file = sys.argv[1]
oligo_strings = parse_oligos(oligo_file)

#flush out data files
path_name = os.path.dirname(sys.argv[1]) 
file_name = os.path.basename(sys.argv[1])[:-len(".fastq")]
data_name = path_name + "/" + file_name + ".paired-oligos.fastq"

with open(data_name, 'w') as f:
    for oligo in oligo_strings:
        f.write(oligo)

