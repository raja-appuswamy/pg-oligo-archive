import os.path
from collections import OrderedDict

def str_freq(s, size):
    """Calculate the relative frequency of every character in the given string.

    The return format is mostly for easy feeding into the Huffman tree creator.

    :s: String.
    :returns: Dictionary of {character: frequency} pairs.

    """

    freqs = OrderedDict()

    for c in s:
        if c in freqs:
            freqs[c] += 1
        else:
            freqs[c] = 1

    # Turn the absolute frequencies into relative ones.
    slen = len(s)
    assert slen == size
    for c in freqs.keys():
        freqs[c] /= size

    return freqs

