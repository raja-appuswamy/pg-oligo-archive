#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import settings
import oligoutil

def complement(oligo):
    complement_map = {'A':'T','G':'C','T':'A','C':'G'}
    return ''.join([complement_map[x] for x in oligo])

def primer_mismatch(primer, oligo):
    assert oligo[0:5] == primer
    seed1 = oligo[0:4]
    seed2 = oligo[1:5]
    seed3 = oligo[2:6]
    seed4 = oligo[3:7]
    seed5 = oligo[4:8]

    rseed1 = complement(seed1)
    rseed2 = complement(seed2)
    rseed3 = complement(seed3)
    rseed4 = complement(seed4)
    rseed5 = complement(seed5)
    rseed11 = oligoutil.rcomplement(seed1)
    rseed21 = oligoutil.rcomplement(seed2)
    rseed31 = oligoutil.rcomplement(seed3)
    rseed41 = oligoutil.rcomplement(seed4)
    rseed51 = oligoutil.rcomplement(seed5)
    print("Checking seed1 " + rseed1 +
            " and seed2 " + rseed2 + 
            " and seed3 " + rseed3 + 
            " and seed4 " + rseed4 + 
            " and seed5 " + rseed5 + 
            " and seed11 " + rseed11 + 
            " and seed21 " + rseed21 + 
            " and seed31 " + rseed31 + 
            " and seed41 " + rseed41 + 
            " and seed51 " + rseed51 + 
            " of primer " +\
            primer + " with oligo " + oligo)

    if rseed1 in oligo[4:]:
        print("Found rseed1")
        return True

    if rseed2 in oligo[5:]:
        print("Found rseed2")
        return True

    if rseed3 in oligo[6:]:
        print("Found rseed3")
        return True

    if rseed4 in oligo[7:]:
        print("Found rseed4")
        return True

    if rseed5 in oligo[8:]:
        print("Found rseed5")
        return True

    if rseed11 in oligo[4:]:
        print("Found rseed11")
        return True

    if rseed21 in oligo[5:]:
        print("Found rseed21")
        return True

    if rseed31 in oligo[6:]:
        print("Found rseed31")
        return True

    if rseed41 in oligo[7:]:
        print("Found rseed41")
        return True

    if rseed51 in oligo[8:]:
        print("Found rseed51")
        return True

    return False

# Need to pick table, column, and db primers such that there can be no hairpin
# secondary structures. 
# UFP-5' -- TABLE -- data -- COL -- DB -- UFP3'
def make_primers():
    pfile = open("primer_list.txt", 'r')
    primers = pfile.readlines()
    pfile.close()
    print(primers)

    #pick table primer
    table_primers = []
    nprimers = 0
    next_free_primer = 0
    while nprimers < settings.G_MAX_TABLES:
        candidate_primer = primers[next_free_primer].strip()
        oligo = candidate_primer + settings.G_5_PRIMER
        if primer_mismatch(candidate_primer, oligo):
            next_free_primer += 1
            if (next_free_primer == len(primers)):
                print("ERROR: No more primers available.")
                sys.exit(1)

        else:
            print("Adding primer " + candidate_primer + " as table primer")
            table_primers.append(primers.pop(next_free_primer).strip())
            nprimers += 1

    # pick db primers
    next_free_primer = 0
    db_primers = []
    nprimers = 0
    g_or_c = ['G','C']
    while nprimers < settings.G_MAX_DBS:
        candidate_primer = primers[next_free_primer].strip()
        oligo = candidate_primer + settings.G_3_PRIMER
        if candidate_primer[0] != g_or_c[nprimers % 2] or primer_mismatch(candidate_primer, oligo):
            next_free_primer += 1
            if (next_free_primer == len(primers)):
                print("ERROR: No more primers available.")
                sys.exit(1)
        else:
            print("Adding primer " + candidate_primer + " as db primer")
            db_primers.append(primers.pop(next_free_primer).strip())
            nprimers += 1

    #now pick col primers
    next_free_primer = 0
    col_primers = []
    nprimers = 0
    while nprimers < settings.G_MAX_COLS:
        print("Checking inedx " + str(next_free_primer) + " of " + str
                (len(primers)))
        candidate_primer = primers[next_free_primer].strip()

        #need to check col for every db primer
        drop_primer = False
        for dbprimer in db_primers:
            oligo = candidate_primer + dbprimer + settings.G_3_PRIMER
            print("Checking cprimer " + candidate_primer + " in oligo " +
                    dbprimer + " ---- " + settings.G_3_PRIMER)
            if primer_mismatch(candidate_primer, oligo):
                drop_primer = True
                break

        if drop_primer:
            print("Dropping primer " + candidate_primer + " as col primer")
            next_free_primer += 1
            if (next_free_primer == len(primers)):
                print("ERROR: No more primers available.")
                sys.exit(1)
        else:
            print("Adding primer " + candidate_primer + " as col primer #" + str(nprimers))
            col_primers.append(primers.pop(next_free_primer).strip())
            nprimers += 1
            next_free_primer = 0
    
    return table_primers, db_primers, col_primers

table_primers, db_primers,col_primers = make_primers()
print("TABLE PRIMERS")
print("-------------")
print(table_primers)

print("DB PRIMERS")
print("-------------")
print(db_primers)

print("COL PRIMERS")
print("-------------")
print(col_primers)
