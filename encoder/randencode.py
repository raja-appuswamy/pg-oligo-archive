import sys
import numpy as np
import random
import re

rand_len = 4
rand_seed = 10
oligo_len = 200
chunk_len = oligo_len*2

def readFileAsBits(file_name):
    #file is read and stored as a string sequence of bits
    file_bytes = np.fromfile(file_name, dtype = "uint8")
    file_bits = np.unpackbits(file_bytes)
    data_list = list(file_bits)
    data = ''.join(str(e) for e in data_list)
    return data

def getIndexAndDataLength(data_len, chunk_len):
    #determine how many bits of a chunk are reserved to index and how many bits are reserved for data
    possible_len = 0
    possible_chunks = 0
    c_index_len = 0
    c_data_len = chunk_len
    while possible_len < data_len:
        c_index_len = c_index_len+1
        c_data_len = c_data_len-1
        possible_chunks = pow(2,c_index_len)
        possible_len = possible_chunks*c_data_len
    return c_index_len, c_data_len

def getChunks(data, chunk_len):
    #split data into chunks of same length
    chunks = [data[i:i+chunk_len] for i in range(0, len(data), chunk_len)]
    nb_chunks = len(chunks)
    chunks[nb_chunks-1] = chunks[nb_chunks-1].ljust(chunk_len,'0')
    return chunks

def addIndexToData(chunks, c_index_len):
    #add index to each chunk then join it inside data
    index = 0
    for i in range (len(chunks)):
        chunks[i] = str(bin(index))[2:].rjust(c_index_len,'0') + chunks[i]
        index = index + 1
    return ''.join(chunks)

def randomizeData(chunks, rand_seed, rand_len):
    #apply xor between random chunks and data chunks to randomize data
    print('Randomizing data...')
    rand_data = ''
    random.seed(rand_seed)
    for i in range (len(chunks)):
        rand = random.randint(1,pow(2,rand_len)-2)
        chunk = int(chunks[i], 2)
        rand_chunk = chunk^rand
        rand_data = rand_data + bin(rand_chunk)[2:].rjust(rand_len,'0')
    return rand_data

def generateOligos(data):
    #generate oligos where each nucleotide is identified babsed on the previous one
    print('Generating oligos...')
    oligo_data = ''
    last_nucleo = 'G'
    nucleo_map = {
        'A':['T','C','G','AC'],
        'T':['C','G','A','TG'],
        'C':['G','A','T','CT'],
        'G':['A','T','C','GA']}
    data_len = len(data)
    for i in range(0,data_len, 2):
        sub = data[i:i+2]
        nucleo = nucleo_map[last_nucleo][int(sub,2)]
        oligo_data = oligo_data + nucleo
        last_nucleo = nucleo[-1]
    oligo_data_len = len(oligo_data)
    oligo_chunks = [oligo_data[i:i+oligo_len] for i in range(0, oligo_data_len,oligo_len)]
    nb_oligos = len(oligo_chunks)
    while len(oligo_chunks[nb_oligos-1])<oligo_len:
        sub = '00'
        last_nucleo = oligo_chunks[nb_oligos-1][-1]
        nucleo = nucleo_map[last_nucleo][int(sub,2)]
        oligo_chunks[nb_oligos-1] = oligo_chunks[nb_oligos-1] + nucleo
        last_nucleo = nucleo[-1]
    return oligo_chunks

def outputOligosToFile(oligos, file_name):
    #output the oligos to a file
    oligo_file_name = file_name.split('.')[0]+'.oligos'
    oligo_file = open(oligo_file_name, 'w')
    oligo_file.write('\n'.join(oligos))
    print('Oligos generated in ' + oligo_file_name)
    oligo_file.close()

if len(sys.argv) < 2:
    print('You must pass the filename of the file to encode: ./randencode <filename>\n')
    sys.exit(1)

file_name = sys.argv[1]
print('Encoding ' + file_name)
#read file into sequence of bits
data = readFileAsBits(file_name)
#identiy required index bits and data bits
c_index_len, c_data_len = getIndexAndDataLength(len(data), chunk_len)
#split data bits into chunks
chunks = getChunks(data, c_data_len)
#add index bits
data = addIndexToData(chunks, c_index_len)
#chunk data for randomization
chunks = getChunks(data, rand_len)
#randomize data
data = randomizeData(chunks, rand_seed, rand_len)
#generate oligos after randomization
oligos = generateOligos(data)
#output the generated oligos to a file
outputOligosToFile(oligos, file_name)
