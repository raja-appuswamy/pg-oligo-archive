#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import statistics
import sys
import os.path
from collections import Counter
from collections import OrderedDict
import oligoutil
import settings
import operator

if len(sys.argv) != 3:
    print('./reseq_rcomplement <input.oligo> <output.oligo>\n')
    sys.exit(1)

def parse_oligos(oligo_file):
    nsense_failure = 0
    fwd_oligos = []
    rev_oligos = []
    noligos = 0
    nforward = 0
    nreverse = 0
    with open(oligo_file, 'r') as f:
        for line in f:
            noligos += 1

            #remove primers and sense NTs
            full_oligo_str = line.rstrip()
            fields = full_oligo_str.split(' ')
            full_oligo_str = fields[1]
            oligo_str = full_oligo_str

            #remove table, db, col primers
            oligo_str = oligo_str[settings.G_TABLE_PRIMER_LEN:
                    -settings.G_DB_PRIMER_LEN]
            oligo_str = oligo_str[:-settings.G_COL_PRIMER_LEN]

            #oligoutil.dprint("Input oligo: " + oligo_str)

            # check sense
            if oligo_str[0] == 'G' or oligo_str[0] == 'C':
                if oligo_str[-1] != 'T' and oligo_str[-1] != 'A':
                    nsense_failure += 1
                    continue

                #for now, we do only forward
                nreverse += 1
                oligo_str = oligoutil.rcomplement(full_oligo_str)
                rev_oligos.append(str(fields[0]) + ' ' + full_oligo_str + "\n")

            elif oligo_str[0] == 'T' or oligo_str[0] == 'A':
                if oligo_str[-1] != 'G' and oligo_str[-1] != 'C':
                    nsense_failure += 1
                    continue
                
                nforward += 1
                fwd_oligos.append(str(fields[0]) + ' ' + full_oligo_str + "\n")


    print("From " + str(noligos) + " oligos, found " +
            str(nsense_failure) + " sense failures, " +
            str(nforward) + " valid forward reads, " + 
            str(nreverse) + " valid reverse reads.")

    return fwd_oligos,rev_oligos

#now decode the oligo file line at a time
fwd_oligos, rev_oligos = parse_oligos(sys.argv[1])

with open(sys.argv[2] + '.fwd', 'w') as f:
    for oligo in fwd_oligos:
        f.write(oligo)

with open(sys.argv[2] + '.rev', 'w') as f:
    for oligo in rev_oligos:
        f.write(oligo)

