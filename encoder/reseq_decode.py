#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import statistics
import sys
import os.path
from collections import Counter
from collections import OrderedDict
import oligoutil
import settings
import operator

if len(sys.argv) != 5:
    print('./decode <fileid map file> <data freq file> <dict freq file> <oligofile>\n')
    sys.exit(1)

def parse_oligos(oligo_file):
    data_oligo_strings = []
    data_strings = []
    dict_strings = []
    lena_strings = []
    nsense_failure = 0
    nparity_failure = 0
    ndecode_failure = 0
    nfileid_failure = 0
    noligos = 0
    nforward = 0
    nreverse = 0
    mezzanine_strings = []
    with open(oligo_file, 'r') as f:
        for line in f:
            noligos += 1
            #remove priers and sense NTs
            #oligo_str = line.rstrip()
            #assert oligo_str[:settings.G_5PRIMER_LEN] == settings.G_5_PRIMER
            #assert oligo_str[-settings.G_3PRIMER_LEN:] == settings.G_3_PRIMER
            #oligo_str = oligo_str[settings.G_5PRIMER_LEN + 1 :
            #        -(settings.G_3PRIMER_LEN + 1)]
            #oligoutil.dprint(oligo_str)


            #remove primers and sense NTs
            oligo_str = line.rstrip()
            oligo_str_cp = oligo_str
            oligoutil.dprint("Input oligo: " + oligo_str)

            #assert oligo_str[:settings.G_5PRIMER_LEN] == settings.G_5_PRIMER
            #assert oligo_str[-settings.G_3PRIMER_LEN:] == settings.G_3_PRIMER
            
            # check sense
            if oligo_str[0] == 'G' or oligo_str[0] == 'C':
                if oligo_str[-1] != 'T' and oligo_str[-1] != 'A':
                    nsense_failure += 1
                    continue
                else:
                    #for now, we do only forward
                    nreverse += 1
                    continue
            elif oligo_str[0] == 'T' or oligo_str[0] == 'A':
                if oligo_str[-1] != 'G' and oligo_str[-1] != 'C':
                    nsense_failure += 1
                    continue
                
                nforward += 1

            #remove sense
            oligo_str = oligo_str[1 :-1]

            #decode metadata (direction trit, fileid trits, parity trit)
            metadata_len = 1 + settings.G_FILEID_CHARS + 1
            oligo_metadata = oligo_str[40:40 + metadata_len]
            oligoutil.dprint("Raw Metadata: " + oligo_metadata)
            try:
                encoded_metadata = oligoutil.decode_oligo(oligo_metadata)
            except:
                ndecode_failure += 1
                continue

            oligoutil.dprint("Encoded metadata: " + encoded_metadata)

            #get and remove file id
            fileid_str = encoded_metadata[1:-1]
            oligoutil.dprint("raw fileid: " + fileid_str)
            assert len(fileid_str) == settings.G_FILEID_CHARS
            fileid = oligoutil.todecimal(fileid_str) 
            if (fileid == 9):
                mezzanine_strings.append(line)
                continue

            if (fileid == 10):
                lena_strings.append(line)
                continue

            if fileid > 10:
                nfileid_failure += 1
                continue

            #decode data
            oligo_str = oligo_str[0:40] + oligo_str[40 + metadata_len: ]
            oligoutil.dprint("raw data: " + oligo_str)

            try:
                if (encoded_metadata[0] == '1'):
                    oligo_str = oligoutil.rcomplement(oligo_str)                
                    oligoutil.dprint("reverse complimenting reversed oligo:" + oligo_str)

                encoded_data = oligoutil.decode_oligo(oligo_str)
                oligoutil.dprint("encoded data: " + encoded_data)
            except:
                ndecode_failure += 1
                continue

            # check and remove parity
            parity_str = encoded_metadata[-1:]
            oligoutil.dprint("raw parity: " + parity_str)
            encoded_metadata = encoded_metadata[:-1]
            if oligoutil.compute_parity(encoded_data + encoded_metadata) != parity_str:
                print('Parity verification failed.')
                print('oligo:' + line.rstrip())
                print('decode data:' + encoded_data)
                print('decode metadata:' + encoded_metadata)
                print('computed parity: ' +
                        str(oligoutil.compute_parity(encoded_data + encoded_metadata)))

                print('stored parity: ' + parity_str)
                #sys.exit(1)
                nparity_failure += 1
                continue
  
            if 'dict' in fileid_map[fileid]:
                dict_strings.append(encoded_data + fileid_str)
            else:
                data_oligo_strings.append(oligo_str)
                data_strings.append(encoded_data + fileid_str)

    print("From " + str(noligos) + " oligos, found " +
            str(nsense_failure) + " sense failures, " +
            str(nfileid_failure) + " fileid failures, " +
            str(nparity_failure) + " parity failurs, " +
            str(ndecode_failure) + " decode failurs, " +
            str(nforward) + " valid forward reads, " + 
            str(nreverse) + " valid reverse reads.")

    return data_oligo_strings, data_strings, dict_strings, lena_strings, mezzanine_strings

def decode_data(data_oligo_strings, data_strings, ternary_huffman):
    encoded_map = {}

    ntotal = 0
    nhuffman_failure = 0
    for i in range(0,len(data_strings)):
        encoded_str = data_strings[i]
        oligo_str = data_oligo_strings[i]
        ntotal += 1
        fileid_str = encoded_str[-settings.G_FILEID_CHARS:]
        encoded_str = encoded_str[: -settings.G_FILEID_CHARS]
        fileid = oligoutil.todecimal(fileid_str) 

	#get base3 length
        base3_str = encoded_str[-settings.G_NBASE3_LEN_CHARS:]
        length = oligoutil.todecimal(base3_str)
        encoded_str = encoded_str[: -settings.G_NBASE3_LEN_CHARS]

	#remove padding
        encoded_str = encoded_str[0 : length]
	
	#huffman decode 
        oligoutil.dprint('Huffman string: ' + encoded_str)
        try:
            decoded_str = ''.join(ternary_huffman.decode(encoded_str))
        except:
            nhuffman_failure += 1
            continue

        oligoutil.dprint('Decoded str: ' + decoded_str)
        if fileid not in encoded_map:
            encoded_map[fileid] = []

        encoded_map[fileid].append(oligo_str)
        encoded_map[fileid].append(decoded_str)

    print("Of " + str(ntotal) + " input data oligos, " +
            str(nhuffman_failure) + " huffman failures found")

    return encoded_map

def decode_dict(dict_strings, ternary_huffman):
    segments = {}

    for encoded_str in dict_strings:
        #get fileid
        fileid_str = encoded_str[-settings.G_FILEID_CHARS:]
        fileid = oligoutil.todecimal(fileid_str) 
        encoded_str = encoded_str[: -settings.G_FILEID_CHARS]

	#get base3 length
        base3_str = encoded_str[-settings.G_NBASE3_LEN_CHARS:]
        length = oligoutil.todecimal(base3_str)
        encoded_str = encoded_str[:-settings.G_NBASE3_LEN_CHARS]

        # get index
        index_str = encoded_str[-settings.G_MAX_INDEX_LEN:]
        chunk_idx = oligoutil.todecimal(index_str)
        if (chunk_idx > 172):
            continue

        encoded_str = encoded_str[:-settings.G_MAX_INDEX_LEN]

	#remove padding
        encoded_str = encoded_str[0 : length]
	
        #add string at index
        if chunk_idx not in segments:
            segments[chunk_idx] = {}

        if encoded_str not in segments[chunk_idx]:
            segments[chunk_idx][encoded_str] = 1
        else:
            segments[chunk_idx][encoded_str] += 1

    #pick segment with highest count
    consensus_segments = {}
    for chunk_idx, oligo_map in segments.items():
        sorted_matches = sorted(oligo_map.items(), key=operator.itemgetter(1),
                reverse=True)
        print("Chunk idx " + str(chunk_idx) + " has " + str(len(oligo_map)) + " entries\n")
        consensus_segments[chunk_idx] = sorted_matches[0][0]

    #now we have all segments. merge it all and decode
    #huffman_str = ''.join(segments[x] for x in sorted(segments))
    huffman_str = ''.join(consensus_segments[x] for x in sorted(consensus_segments))
    oligoutil.dprint('Length of encoded str: ' + str(len(huffman_str)))

    decoded_bytes = ternary_huffman.decode(huffman_str)

    return bytearray(int(c) for c in decoded_bytes)

data_freq_file = ''
dict_freq_file = ''
fileid_map_file = ''
oligo_file = ''
for arg in sys.argv[1:]:
    if 'dict.freqs' in arg:
        dict_freq_file = arg
    elif 'data.freqs' in arg:
        data_freq_file = arg
    elif 'map' in arg:
        fileid_map_file = arg
    elif 'oligos' in arg:
        oligo_file = arg
    elif 'fastq' in arg:
        oligo_file = arg
    else:
        print('Invalid file ' + arg)
        sys.exit(1)

#load huffman dictionary
print('Loading huffman for dict file: ' + dict_freq_file)
dict_freqs, ternary_huffman_dict = oligoutil.load_huffman_dictionary(dict_freq_file)

print('Loading huffman for data file: ' + data_freq_file)
data_freqs, ternary_huffman_data = oligoutil.load_huffman_dictionary(data_freq_file)

#load file id map
print('Loading fileid map from: ' + fileid_map_file)
fileid_map = oligoutil.deserialize_dictionary(fileid_map_file)

#now decode the oligo file line at a time
data_oligo_strings, data_strings, dict_strings, lena_strings, mezzanine_strings = parse_oligos(oligo_file)
print('Found ' + str(len(data_strings)) + ' data strings, ' +
        str(len(dict_strings)) + ' dict strings, ' +
        str(len(lena_strings)) + ' lena strings, and ' + 
        str(len(mezzanine_strings)) + ' mezzanine strings')

decoded_data = decode_data(data_oligo_strings, data_strings, ternary_huffman_data)
decoded_dict  = decode_dict(dict_strings, ternary_huffman_dict)

#flush out data files
data_files = []
path_name = os.path.dirname(sys.argv[1]) 
for key, value in decoded_data.items():
    base_name = os.path.basename(fileid_map[key])
    data_files.append(base_name[0:base_name.find('.')])
    file_name = path_name + '/' + fileid_map[key] + '.recovered'
    print('Writing decoded file: ' + file_name)
    with open(file_name, 'w') as f:
        for line in value:
            f.write(line + '\n')

#flush out dictionary
dict_file_name = path_name + '/' + '-'.join(data_files) + '.dict.recovered'
print('Writing decoded dict: ' + dict_file_name)
f = open(dict_file_name, 'wb')
f.write(decoded_dict)
f.close()

#flush out lena and mezzine if found
if (len(lena_strings)):
    lena_name = path_name + '/lena.recovered.oligos'
    print('Writing lena oligos to ' + lena_name)
    with open(lena_name, 'w') as f:
        for oligo in lena_strings:
            f.write(oligo)

if (len(mezzanine_strings)):
    mezzanine_name = path_name + '/mezzanine.recovered.oligos'
    print('Writing mezzanine oligos to ' + mezzanine_name)
    with open(mezzanine_name, 'w') as f:
        for oligo in mezzanine_strings:
            f.write(oligo)

