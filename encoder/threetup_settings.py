#!/usr/bin/env python3
# -*- coding: utf-8 -*-

G_NHUFFMAN_CHARS = 70
G_NBASE3_LEN_CHARS = 4
G_GC_LOW = 0.2
G_GC_HIGH = 0.7
G_LOG_LEVEL = 1
