#!/usr/bin/env python3
import sys
sys.path.append("../encoder")
import settings

if len(sys.argv) < 3:
    print('./exact-match.py <original-oligos> <resequenced-file>')
    sys.exit(1)

oligo_map = {}

#bulid map after removing primers
with open(sys.argv[1]) as f:
    for line in f:
        oligo_str = line.rstrip()
        oligo_str = oligo_str[settings.G_5PRIMER_LEN :
                -(settings.G_3PRIMER_LEN)]
        oligo_map[oligo_str] = 0

print("Loaded " + str(len(oligo_map)) + " input oligos.")
print("Building histogram now...")

spurious_map = {}
nreads = 0
with open(sys.argv[2]) as f:
    for line in f:
        nreads += 1
        oligo_str = line.rstrip()
        if ' ' in oligo_str:
            fields = oligo_str.split(' ')
            oligo_str = fields[1]
            count = int(fields[0])
        else:
            count = 1

        if oligo_str in oligo_map:
            oligo_map[oligo_str] += count
        elif oligo_str in spurious_map:
            spurious_map[oligo_str] += count
        else:
            spurious_map[oligo_str] = count

missing_oligos = []
nmatches = 0
hist_output = open("oligo-exact-match.hist", 'w')
oligo_id = 0
for key,value in oligo_map.items():
    oligo_id += 1
    if value == 0:
        missing_oligos.append(key)
    else:
        nmatches += value
    hist_output.write(str(oligo_id) + "," + key + "," + str(value) + "\n")

hist_output.close()

nspurious = 0
for key,value in spurious_map.items():
    nspurious += value

if len(missing_oligos) > 0:
    print("Out of " + str(len(oligo_map)) + " oligos, " + str(len(missing_oligos))
            + " were not found even once")
else:
    print("Found all oligos!")
    print("Out of " + str(nreads) + " reads, found " + str(nmatches) + 
        " exact matches and " + str(nspurious) + " spurious reads")
