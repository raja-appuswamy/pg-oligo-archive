#!/bin/bash

# Program to calculate hamming distance
# between 2 given strings

main() {
	
	if [ $# -eq 0 ] || [ $# -eq 1 ]; then
		echo "Usage: hamming.sh <string1> <string2>"
		exit 1
    else
        first=$1
        second=$2
        distance=0

        if [ ${#first} -ne ${#second} ]; then
            echo "left and right strands must be of equal length"
            exit 1
        fi

        for (( i=0; i<${#first}; ++i )); do
            if [ ${first:$i:1} != ${second:$i:1} ]; then
                let "distance++"
            fi
        done

        echo $distance
	fi

}

main "$@"
