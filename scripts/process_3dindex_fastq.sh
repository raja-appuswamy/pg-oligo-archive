#!/bin/bash

IN1=$1
IN2=$2
PGOA_PATH=/home/eurecom/dna-storage/pg-oligo-archive

#assemble
#./pear -f ../../resequenced-fastq/Undetermined_S0_R1_001.fastq.gz -r ../../resequenced-fastq/Undetermined_S0_R2_001.fastq.gz -o ../../resequenced-fastq/pear-merged
#or
~/bbmap_38.81/bbmerge.sh in=$IN1 in2=$IN2 out=merged.fastq outu=bbunmerged.fastq
rm bbunmerged.fastq
#or
~/cope-v1.2.5/cope -a $IN1 -b $IN2 -o cope-merged;
cat cope-merged.connect.fq >> merged.fastq;
rm *.fq;

# get only 110nts
awk '(NR % 4==2 && length($0)==110){print $0}' merged.fastq > only110nts.fastq
#awk '(NR % 4==2){print $0}' merged.fastq > only110nts.fastq
rm merged.fastq

#sed -r 's/^\s+//' only110nts.fastq > only110nts.quantity.sorted.fastq;
#echo sorting;
sort -S10G only110nts.fastq | uniq -c > only110nts.sorted.fastq;
rm only110nts.fastq;

#echo count sorting;
sed -r 's/^\s+//' only110nts.sorted.fastq | sort -r -n -k1 -S10G > only110nts.quantity.sorted.fastq;
rm only110nts.sorted.fastq;

##following two commands gets rid of oligos that are not covered atleast twice (or keep all if you want)
#get rid of Ns and convert all valid reverse complements into forwrad.
#This also gets rid of oligos with invalid sense trits 
#echo reformatting;
awk '$1 > 10 {print $0}' only110nts.quantity.sorted.fastq | grep -v 'N' > only110nts.multiplematches.quantity.sorted.allgood.fastq
#grep -v 'N' only110nts.quantity.sorted.fastq > only110nts.multiplematches.quantity.sorted.allgood.fastq;
rm only110nts.quantity.sorted.fastq;

~/dna-storage/pg-oligo-archive/encoder/reseq_3dindex_rcomplement.py only110nts.multiplematches.quantity.sorted.allgood.fastq only110nts.multiplematches.quantity.sorted.allgood.no-rcomplements.fastq 
rm only110nts.multiplematches.quantity.sorted.allgood.fastq;

cut -f2 -d' ' only110nts.multiplematches.quantity.sorted.allgood.no-rcomplements.fastq.fwd > only110nts.multiplematches.quantity.sorted.allgood.no-rcomplements.oligos
rm only110nts.multiplematches.quantity.sorted.allgood.no-rcomplements.fastq.rev;
rm only110nts.multiplematches.quantity.sorted.allgood.no-rcomplements.fastq.fwd;

#now after this, run the decoder
 ~/dna-storage/pg-oligo-archive/encoder/3dindex_decoder.py ~/dna-storage/pg-oligo-archive/data/3dindex_larger_input/*.freqs ~/dna-storage/pg-oligo-archive/data/3dindex_larger_input/*.map only110nts.multiplematches.quantity.sorted.allgood.no-rcomplements.oligos > 3dindexdecoder.debug.log 2>&1;
rm only110nts.multiplematches.quantity.sorted.allgood.no-rcomplements.oligos;

#now validation
#(for f in `ls data/3dindex_larger_input/syn*.recovered`; do sort $f | uniq -c | awk '$1 > 1{print $2}' > $f.unique; echo scripts/reassemblecols.py $f.unique ${f/.recovered/}; scripts/reassemblecols.py $f.unique ${f/.recovered/}; echo "------------------------"; done) > validation.log
