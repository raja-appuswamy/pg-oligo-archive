cd /media/ssd/oligoarchive/2021-random-access-seq-data/201211_gen_oligoArchive;
for c in {Uni,A,B,C,D,E,F,G,H}; do 
	echo cd ${c}dir-3-RC; 
	cd ${c}dir-3-RC; 
	echo rm ~/dna-storage/pg-oligo-archive/data/3dindex_larger_input/*.recovered;
	rm ~/dna-storage/pg-oligo-archive/data/3dindex_larger_input/*.recovered; rm ~/dna-storage/pg-oligo-archive/data/3dindex_larger_input/*unique;
	echo ~/dna-storage/pg-oligo-archive/scripts/process_3dindex_fastq.sh *R1_001.fastq *R2_001.fastq;
	~/dna-storage/pg-oligo-archive/scripts/process_3dindex_fastq.sh *R1_001.fastq *R2_001.fastq;
	cd ~/dna-storage/pg-oligo-archive;
	echo "Doing validation"
	(for f in `ls data/3dindex_larger_input/syn*.recovered`; do sort $f | uniq -c | awk '$1 > 0{print $2}' > $f.unique; echo scripts/reassemblecols.py $f.unique ${f/.recovered/}; scripts/reassemblecols.py $f.unique ${f/.recovered/}; rm $f.unique; echo "------------------------"; done) > ${c}dir3RC.validation.log
	cd /media/ssd/oligoarchive/2021-random-access-seq-data/201211_gen_oligoArchive;
	echo "Done"
done
