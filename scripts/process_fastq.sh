#!/bin/bash

#assemble
./pear -f ../../resequenced-fastq/Undetermined_S0_R1_001.fastq.gz -r ../../resequenced-fastq/Undetermined_S0_R2_001.fastq.gz -o ../../resequenced-fastq/pear-merged
#or
 ~/bbmap_38.81/bbmerge.sh in=Adir-3-RC_S17_R1_001.fastq in2=Adir-3-RC_S17_R2_001.fastq out=Adir-3-RC_S17_merged.fastq outu=Adir-3-RC_S17_unmerged.fastq

# get only 91nts
awk '(NR % 4==2 && length($0)==91){print $0}' ../../resequenced-fastq/pear-merged.assembled.fastq > ../../resequenced-fastq/only91nts.fastq

echo sorting;
sort -S10G only91nts.fastq | uniq -c > only91nts.sorted.fastq;

echo count sorting;
sed -r 's/^\s+//' only91nts.sorted.fastq | sort -r -n -k1 -S10G > only91nts.quantity.sorted.fastq;

##following two commands gets rid of oligos that are not covered atleast twice
#echo reformatting;
#awk '$1 > 1 {print $0}' only91nts.quantity.sorted.fastq | grep -v 'N' > only91nts.multiplematches.quantity.sorted.allgood.fastq
#cut -f2 -d' ' only91nts.multiplematches.quantity.sorted.allgood.fastq > only91nts.multiplematch.allgood.oligos

#the following command gives all valid oligos.. even those found only once
#cut -f2 -d' ' only91nts.quantity.sorted.fastq | grep -v 'N' > only91nts.quantity.sorted.allgood.fastq

#get rid of Ns and convert all valid reverse complements into forwrad.
#This also gets rid of oligos with invalid sense trits 
grep -v 'N' ../../resequenced-fastq/only91nts.quantity.sorted.fastq > ../../resequenced-fastq/only91nts.quantity.noNs.fastq
/reseq_rcomplement.py ../../resequenced-fastq/only91nts.quantity.sorted.noNs.fastq ../../resequenced-fastq/only91nts.quantity.sorted.noNs.no-rcomplements.fastq
cut -f2 -d' ' ../../resequenced-fastq/only91nts.quantity.sorted.noNs.no-rcomplements.fastq > ../../resequenced-fastq/only91nts.quantity.sorted.noNs.no-rcomplements.onlyoligos.fastq

#now after this, run the decoder

#for verification, you can take the output from the decoder, i.e the recovered file, do sort uniq on it and then call reassemblecols

#when decoder spits out lena and mezzanine oligos, we can use join to get the corresponding counts
join -o 2.1 2.2 -2 2 <(sort mezzanine.recovered.oligos) <(sort -k2 ../../resequenced-fastq-with-phage/pear-merged/noNs.countsorted.unique.only91nts.assembled.fastq) > ../../resequenced-fastq-with-phage/pear-merged/mezzanine.joinrecovered.noNs.countsorted.unique.only91nts.assembled.fastq


#quick and dirty way of getting consensus
for f in `ls ../data/*.recovered`; do
    echo Checking $f for consensus
    awk 'NR % 2 == 0 {print $0}' $f | sort | grep -v -e '^|' | uniq -c | sed -r 's/^\s+//' | sort -k1 -r | awk '$0 > 2 {print $0}' | sort -k2
done
