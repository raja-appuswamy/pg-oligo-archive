#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys

#file that checks if recover was ok. we reassemble columns from reads. 
# then we break original .base file into columns.
# then we compare the two.
if len(sys.argv) < 3:
    print('./reassemblecols.py <processed-read-file> <table-base-file>')
    sys.exit(1)


col_map = {}

with open(sys.argv[1]) as f:
    for line in f:
        fields = line.strip().split('|')
        if fields[0] not in col_map:
            col_map[fields[0]] = []
        try:
            col_map[fields[0]].extend([val for val in fields[1:]])
        except:
            #do nothing
            continue

#for key in sorted(col_map):
#    col_map[key].sort()
#    print(str(key) + "|" + "|".join([str(i) for i in col_map[key]]))
#    print("================")

base_cols = {}
base_file = open(sys.argv[2], "r")
base_lines = base_file.readlines()
nlines = 0

for line in base_lines:
    cols = line.strip().split("|")
    for i in range(len(cols)):
        if nlines == 0:
            base_cols[str(i)] = []
        try:
            base_cols[str(i)].append(cols[i])
        except:
            continue

    nlines += 1

for key in sorted(base_cols):
    base_cols[key].sort()
    missfrac = 0
    excessfrac = 0
    if key not in col_map:
        print("Column id " + str(key) + " not found in reads")
        missfrac = 100
    else:
        col_map[key].sort()
        #print("Column values")
        #print("-------------")
        #print("Base values")
        #print(base_cols[key])
        #print("Read values")
        #print(col_map[key])
        #print("-------------")
        base_set = set(base_cols[key])
        read_set = set(col_map[key])
        if ((base_set - read_set) != set()):
            print("Missing some base entries in read for column " + str(key))
            print("Base - read for column " + str(key))
            print(set(base_cols[key]) - set(col_map[key]))
            #print("Miss ratio: " + str((bslen - rslen) * 100/bslen) + "%")
            missfrac = len(base_set - read_set) * 100/len(base_set)
        elif ((read_set - base_set) != set()):
            print("Read spurious extra for column " + str(key))
            print("Read - base for column " + str(key))
            print(set(col_map[key]) - set(base_cols[key]))
            #print("Excess ratio: " + str((rslen - bslen) * 100/bslen) + "%")
            excessfrac = len(read_set - base_set) * 100/len(base_set)
        else:
            print("Col id " + str(key) + " matches read and base")

    #print("================")
    print(sys.argv[2] + "," + str(key) + "," + str(missfrac) + "," + str(excessfrac))

    #print(str(key) + "|" + "|".join([str(i) for i in base_cols[key]]))
