#!/usr/bin/env python3

import sys
import operator
import os

if len(sys.argv) < 2:
    print('./reformat.py <filename>')
    sys.exit(1)

str_dict = {}
def isFloat(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

input_files = []
for arg in sys.argv[1:]:
    base_name = os.path.basename(arg)
    input_files.append(base_name[:base_name.find('.')])

# first build dictionary
for idx in range(1,len(sys.argv)):
    print('processsing file: ' + sys.argv[idx])
    base_name = sys.argv[idx] + '.base'
    base_file = open(base_name, 'w')

    with open(sys.argv[idx]) as f:
        for line in f:
            columns = line.rstrip().split('|')
            for i in range(len(columns)):
                if not isFloat(columns[i]) and not columns[i].isnumeric() and len(columns[i]) > 2:
                    if (columns[i] in str_dict):
                        str_dict[columns[i]] += 1
                    else:
                        str_dict[columns[i]] = 1

sorted_str_list = sorted(str_dict.items(), key=operator.itemgetter(1, 0),
        reverse=True)
sorted_dict = {}

str_idx = 0
for key,value in sorted_str_list:
    sorted_dict[key] = str(str_idx)
    str_idx += 1

#now reformat the files
for idx in range(1,len(sys.argv)):
    print('converting file: ' + sys.argv[idx])
    base_name = sys.argv[idx] + '.base'
    base_file = open(base_name, 'w')

    with open(sys.argv[idx]) as f:
        for line in f:
            columns = line.rstrip().split('|')
            for i in range(len(columns)):
                if not isFloat(columns[i]) and not columns[i].isnumeric() and len(columns[i]) > 2:
                    assert columns[i] in sorted_dict
                    columns[i] = sorted_dict[columns[i]]

            new_tuple = '|'.join(columns) + '\n'

            base_file.write(new_tuple)
        
    base_file.close()

#write dictionary
dict_name = os.path.dirname(sys.argv[1]) + '/' + '-'.join(input_files) + '.dict'
dict_file = open(dict_name, 'w')
for key,value in sorted_str_list:
    dict_file.write(key + '\n')
dict_file.close()

