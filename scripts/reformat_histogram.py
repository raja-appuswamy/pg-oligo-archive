#!/usr/bin/env python3

import sys
import operator
import os

if len(sys.argv) < 2:
    print('./reformat.py <filename>')
    sys.exit(1)

str_dict = {}
def isFloat(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

input_files = []
for arg in sys.argv[1:]:
    base_name = os.path.basename(arg)
    input_files.append(base_name[:base_name.find('.')])

#build frequence-based dictionary
nunique_strings = 0
ndeduped_strings = 0
ntotal_strings = 0

print('Building dictionary')
dict_name = os.path.dirname(sys.argv[1]) + '/' + '-'.join(input_files) + '.dict'
dict_file = open(dict_name, 'w')
histogram = {}
for idx in range(1,len(sys.argv)):
    print('Adding file '+ sys.argv[idx] + ' to dictionary')
    with open(sys.argv[idx]) as f:
        for line in f:
            columns = line.rstrip().split('|')
            for i in range(len(columns)):
                if not isFloat(columns[i]) and not columns[i].isnumeric():
                    #don't try to convert string columns that are < 2 chars long
                    if len(columns[i]) > 2:
                        ntotal_strings += 1
                        if columns[i] not in histogram:
                            histogram[columns[i]] = 1
                        else:
                            histogram[columns[i]] += 1
                            ndeduped_strings += 1

#sort histogram based on frequency
sorted_histogram = sorted(histogram.items(), key=operator.itemgetter(1),
        reverse=True)

#assign ids to strings based on sorted histogram. most frequent strings get
#smallest count
idx = 0
prev_freq = sys.maxsize
for item in sorted_histogram:
    if prev_freq < histogram[item[0]]:
        print('found item of high freq in dec. freq histogram')
        sys.exit(1)

    prev_freq = histogram[item[0]]
    histogram[item[0]] = idx
    idx += 1

#now remap the files
for idx in range(1,len(sys.argv)):
    print('processsing file: ' + sys.argv[idx])
    base_name = sys.argv[idx] + '.base'
    base_file = open(base_name, 'w')

    tuples = []
    with open(sys.argv[idx]) as f:
        for line in f:
            columns = line.rstrip().split('|')
            first_position = 0
            for i in range(len(columns)):
                if not isFloat(columns[i]) and not columns[i].isnumeric():
                    if len(columns[i]) >= 2:
                        if (columns[i] not in histogram):
                            print('Found column: ' + columns[i] + ' not in dict.')
                            sys.exit(1)

                        columns[i] = str(histogram[columns[i]])

                    #if (first_position == 0):
                    #    first_position = int(columns[i])
                    #else:
                    #    columns[i] = str(int(columns[i]) - first_position)

            #print(line)
            new_tuple = '|'.join(columns) + '\n'
            #print(new_tuple)

            base_file.write(new_tuple)
            tuples.append(new_tuple)

    base_file.close()

    N_SMALLEST = 2
    for i in range(N_SMALLEST):
        min_tup = min(tuples)
        print(min_tup)
        tuples.remove(min_tup)

#write dictionary
#sorted_str_list = sorted(str_dict.items(), key=operator.itemgetter(1))
#print(sorted_str_list)
for value in sorted_histogram:
    #print(tup[0])
    dict_file.write(value[0] + '\n')

print('Total strings in input: ' + str(ntotal_strings) + ' ,deduped strings: ' +
        str(ndeduped_strings))

dict_file.close()
