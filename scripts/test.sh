#!/bin/bash
#INPUT_DIR=data/sf-twotuples
INPUT_DIR=data/sf-0.0001
make
make encode
make decode

for f in `ls ${INPUT_DIR}/*.tbl.base`; do
    orig=$f; rec=${f}.recovered;
    #diff $orig $rec > /dev/null; 
    #    if [ $? -ne 0 ]; then
    #        echo "Diff $orig $rec failed!";
    #        exit 1
    #    fi
done

echo "Testing done. Encoding + decoding successful!"
